# Automating Networks With Ansible
> This repo will host examples and notes while following along with `Automating Networks with Ansible the Right Way` by `Nick Russo` on Pluralsight


## Setup
---
- This project will use `inv.yml` for the inventory config

## Playbooks
---
> The playbooks written in here are just examples, unless you set up your own lab with ios devices to communicate with
- `vrf_getter.yml` - simple playbooks with tasks for retrieving vrf info with `ios_command`
- `ios_config_set.yml` - simple playbook that sends a config template to a device
- `cli_config_set.yml` - platform agnostic playbook that sends config to device, notice the usage of groups and groupvars here


## Custom Filter
---
> Filters are functions written in python  
> `filter_plugins` need to be defined in `ansible.cfg`
- `get_as_from_rt.yml` : playbook that uses `bgp_as_from_rt` filter to return AS numbers from route targets
  - `plugins/filter/filter.py` : hosts customer filters, refer to `bgp_as_from_rt` as an example

Example 
```

def f_upper(text):
  return text.upper()


def filters():
  return {
          'upper',f_upper
         }
```



## Class Materials
---
- `automating-networks-ansible-right-way/` : directory hosting all materials from pluralsight class


## References
---
- https://app.pluralsight.com/courses/44a808cb-ffc0-447a-92f7-87eb45fb3453/table-of-contents